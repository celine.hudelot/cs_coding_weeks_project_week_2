# CentraleSupélec coding weeks : semaine 2


La semaine 2 des coding weeks commence ce lundi. L'objectif de cette deuxième semaine est d'appliquer les méthodologies et les technologies découvertes lors de la première semaine à un nouveau projet qu'il vous faudra définir et pour lequel vous travaillerez par groupe de 4 à 6 personnes.

Lors de la première 1/2 journée, il vous faudra donc au sein de chaque groupe projet CS weeks :

 + Vous répartir en équipe de 4 à 6 personnes qui travailleront sur un même projet ou sur une même composante d'un projet. Un binôme de la semaine 1 peut tout à fait être dans une même équipe mais ce n'est pas obligatoire.

 + En équipe, choisir et/ou préciser un projet en phase avec la thématique de votre groupe CS weeks et ceci pour vous permettre de capitaliser au mieux sur les apports de la semaine 1. 

 + Ce projet peut être ambitieux et non terminé à l'issue de la semaine 2 mais il vous faudra pouvoir démontrer d'un [MVP](https://medium.com/creative-wallonia-engine/un-mvp-nest-pas-une-version-simplifi%C3%A9e-de-votre-produit-89017ac748b0) (produit minimum viable) en fin de semaine (le temps de développement correspond donc à gros grain à celui que vous avez eu en semaine 1).

 + Pour chaque projet, il faudra commencer, comme sur les différents projets de la semaine 1, avec une première phase d'**analyse** et de **réflexion sur la conception** qui devrait vous permettre d'organiser le travail de la semaine.

 + Les différents sujets de projets avec quelques idées et références vous sont rappelés [ici](./sujetsweeks2.md)
 
Vous serez accompagnés, lors de cette semaine, d'1 ou 2 étudiants de 3ème année de l'option ISIA (Ingénièrie des Systèmes Informatiques et Avancés) de CentraleSupélec. Comme pour vous, leur participation au coding weeks est **une activité pédagogique** qui a pour objectif de les mettre en situation de *coachs* de projets de développement informatique. Ils ne participeront donc pas aux travaux de développement de votre projet. Ils ne sont donc pas forcement des experts techniques et ne pourront pas forcement répondre à toutes vos questions. Une partie de l'équipe d'encadrants de la première semaine sera présente physiquement et à distance via le slack pour les aspects plus techniques.



## Evaluation du projet de la semaine 2

Votre projet de la semaine 2 sera évalué lors de soutenances qui auront lieu le vendredi 23 après midi (le planning est en cours de réalisation).
Pour cette soutenance, il vous faudra donc préparer une présentation incluant :

 + Une description du produit (ce que vous aurez réalisé à l'issue de la semaine) : rappel de l'objectif, de la finalité, petite démonstration et montrer quelques indicateurs de qualité du code :
 	+ Modularité (découpage en modules, fonctions)
 	+ Commentaires
 	+ Couverture de tests
 	+ Paramétrage (les entrées sont-elles des paramètres du système ou non)

+ Le comment ? Il faudra nous montrer et nous expliquer votre découpage en sous-tâches.
+ Qui ? Qui a fait quoi ? Comment le travail a t'il été réparti dans le groupe.

L'ensemble de ces critères seront pris en compte dans l'évaluation.











