# Liste des projets des Coding weeks : semaine 2


## CODING WEEK 01- Game Jam et réalité virtuelle.

Ce projet consiste à programmer un jeu en réalité virtuelle. Cette deuxième semaine consiste en une [game jam](https://fr.wikipedia.org/wiki/Game_jam)  dont l’objectif est de produire un petit projet en réalité virtuelle de votre choix. Ce type d’événement est un concours en temps limité pour produire un jeu innovant dans un état d’avancement bien sûr limité mais pour autant montrant l’originalité des mécanismes mis en jeu. Outre l’aspect ludique de ce type de mini concours, vous pourrez vous confronter à des technologies innovantes en réalité virtuelle. Un petit jury sera constitué pour élire le gagnant d’une semaine qui s’annonce passionnante ! 



## CODING WEEK 02- Game Jam et réalité virtuelle.

Ce projet consiste à programmer un jeu en réalité virtuelle. Cette deuxième semaine consiste en une [game jam](https://fr.wikipedia.org/wiki/Game_jam)  dont l’objectif est de produire un petit projet en réalité virtuelle de votre choix. Ce type d’événement est un concours en temps limité pour produire un jeu innovant dans un état d’avancement bien sûr limité mais pour autant montrant l’originalité des mécanismes mis en jeu. Outre l’aspect ludique de ce type de mini concours, vous pourrez vous confronter à des technologies innovantes en réalité virtuelle. Un petit jury sera constitué pour élire le gagnant d’une semaine qui s’annonce passionnante ! 



## CODING WEEK 03- Une application mobile pour améliorer le quotidien sur les campus CentraleSupélec

Ce projet consiste à programmer une **application pour améliorer la vie quotidienne sur les campus de CentraleSupélec**. 

Lors de votre première semaine qui s'est déroulée sur le campus de Metz, vous avez découvert 
la programmation objet et outils et langages nécessaires pour réaliser une application mobile Android (i.e. une application de Trivial Pursuit).

Cette semaine, sur le campus de Gif, vous allez mettre en application les connaissances acquises par la conception d'une application de votre choix visant à améliorer la vie quotidienne des étudiants et des personnels sur les différents campus de CentraleSupélec. 

Quelques idées : 

+ Participer au projet iFind, proposé par votre camarade [Tarek Zighed](Tarek.Zighed@supelec.fr) et qui vise à developper une application permettant de mettre en relation les personnes qui ont trouvé ou perdu des effets personnels sur les différents campus.

+ Une application de type réseau de voisinage (e.g. [smiile](https://www.smiile.com/)) mais à l'échelle d'un campus

+ Amélioration de [Time Out](https://play.google.com/store/apps/details?id=fr.centralesupelec.bde.timeout&hl=en_US), l'application qui permet d'avoir une actualité sur l'ensemble des évènements au sein du campus.

+ ...

Un jury, composé de personnels et d'étudiants élira la meilleure application.

## CODING WEEK 04- Applications mobiles au service de la planète ! 


Ce projet consiste à programmer **une application mobile pour la planète**.

Lors de votre première semaine qui s'est déroulée sur le campus de Metz, vous avez découvert la programmation objet et outils et langages nécessaires pour réaliser une application mobile Android (i.e. une application de Trivial Pursuit).

La deuxième semaine, sur le campus de Gif, consiste en une mise en application des connaissances acquises par la conception d'une application de votre choix visant à préserver notre planète. Quelques exemples de telles applications sont [ici](https://bethesdagreen.org/mobile-apps-to-help-save-the-planet/). 

Un jury élira la meilleure application.



## CODING WEEK 05 - Open Data Paris Saclay Challenge


Ce projet consiste à **valoriser les données ouvertes de Paris Saclay**, accessible via le [portail Open Data de Paris Saclay](https://opendata.paris-saclay.com/pages/accueil/) par leur analyse et leur visualisation interactive. 

Lors de la première semaine, au travers du projet Twitter, vous avez appris à collecter et récupérer des données du WEB (avec [tweepy](http://www.tweepy.org/) ou avec la récupération de fichiers [json](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation)), à faire quelques analyses simples et opérations de traitement du langage naturel (avec [pandas](https://pandas.pydata.org/) et [Textblob](https://textblob.readthedocs.io/en/dev/)) et à produire des visualisations des résultats de vos analyses avec [matplotlib](https://matplotlib.org/), [seaborn](https://seaborn.pydata.org/) et [dash](https://plot.ly/products/dash/). 

D'autres outils et bibliothèques python comme [scrapy](https://scrapy.org/) pour la collecte de données du WEB, [spacy](https://spacy.io/) pour le traitement du langage natuel ou  [scikit-learn](https://spacy.io/) pour l'analyse de données et l'apprentissage à partir de données ont aussi pu être utilisées.


Cette semaine consiste en la mise en applications des connaissances acquises lors de la première semaine pour **valoriser les données ouvertes de Paris Saclay** de votre choix. Dans un premier temps, chaque équipe choisira un ou plusieurs jeux de données parmi ceux disponibles sur le portail [Open Data de Paris Saclay](https://opendata.paris-saclay.com/pages/accueil/). Ces données pourront être complétées, si nécessaire, par des données provenant d'autres plates-formes institutionnelles de données ouvertes. L'objectif de votre projet est de mettre en valeur ces données au travers d'un chaîne simple d'analyse de ces données et des visualisations de ces dernières.

A titre d'exemples, quelques projets d'étudiants sur des challenges du même type dans d'autres écoles d'ingénieurs :

 + [Nutriscore à partir des données de OpenFoodFacts](http://lig-tdcge.imag.fr/~sylvain/opendata/2018-2019/duboillm-elkabira-hamme-morel2-walgerp/rapport.html)
 + [Pollution dans une ville](http://lig-tdcge.imag.fr/~sylvain/opendata/2017-2018/bassonj-foreyn-noronhat-toumimax/index.html)
 + ...

A l'issue de la semaine, vous ferez une démonstration d'un prototype fonctionnel devant un jury qui élira le meilleur projet.



## CODING WEEK 06 - Dataconnexion (incluant FORUM)


A l'image du concours [Dataconnexion](https://www.etalab.gouv.fr/dataconnexions-6-decouvrez-les-laureats), organisé par la mission [Etalab](https://www.etalab.gouv.fr/), ce projet consiste en la conception d'une application qui réutilise au moins un jeu de données publiques issu du [portail des données ouvertes du gouvernement data.gouv](https://www.data.gouv.fr/fr/) ou d’un portail local.

Lors de la première semaine, au travers du projet Twitter, vous avez appris à collecter et récupérer des données du WEB (avec [tweepy](http://www.tweepy.org/) ou avec la récupération de fichiers [json](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation)), à faire quelques analyses simples et opérations de traitement du langage naturel (avec [pandas](https://pandas.pydata.org/) et [Textblob](https://textblob.readthedocs.io/en/dev/)) et à produire des visualisations des résultats de vos analyses avec [matplotlib](https://matplotlib.org/), [seaborn](https://seaborn.pydata.org/) et [dash](https://plot.ly/products/dash/). 

D'autres outils et bibliothèques python comme [scrapy](https://scrapy.org/) pour la collecte de données du WEB, [spacy](https://spacy.io/) pour le traitement du langage natuel ou  [scikit-learn](https://spacy.io/) pour l'analyse de données et l'apprentissage à partir de données ont aussi pu être utilisées.

Cette semaine consiste en la mise en applications des connaissances acquises lors de la première semaine pour valoriser des données ouvertes par leur analyse et leur visualisation. Dans un premier temps, chaque équipe choisira un ou plusieurs jeux de données parmi ceux disponibles sur les portails Open Data du gouvernement puis developpera l'application permettant de les valoriser auprès du grans public.

A titre d'exemples, quelques projets d'étudiants sur des challenges du même type dans d'autres écoles d'ingénieurs :

 + [Nutriscore à partir des données de OpenFoodFacts](http://lig-tdcge.imag.fr/~sylvain/opendata/2018-2019/duboillm-elkabira-hamme-morel2-walgerp/rapport.html)
 + [Pollution dans une ville](http://lig-tdcge.imag.fr/~sylvain/opendata/2017-2018/bassonj-foreyn-noronhat-toumimax/index.html)
 + ...


A l'issue de la semaine, vous ferez une démonstration d'un prototype fonctionnel devant un jury qui élira le meilleur projet.


## CODING WEEK 07 - Monitorer la marque CentraleSupélec sur les médias sociaux


Ce projet consiste à programmer un outil de visualisation de la marque CentraleSupélec sur les réseaux sociaux et en particulier sur Twitter à l'image des travaux de Christopher G Healey disponible [ici](https://www.csc2.ncsu.edu/faculty/healey/tweet_viz/tweet_app/). 


Lors de la première semaine, au travers du projet Twitter, vous avez appris à collecter et récupérer des données du WEB (avec [tweepy](http://www.tweepy.org/) ou avec la récupération de fichiers [json](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation)), à faire quelques analyses simples et opérations de traitement du langage naturel (avec [pandas](https://pandas.pydata.org/) et [Textblob](https://textblob.readthedocs.io/en/dev/)) et à produire des visualisations des résultats de vos analyses avec [matplotlib](https://matplotlib.org/), [seaborn](https://seaborn.pydata.org/) et [dash](https://plot.ly/products/dash/). 

D'autres outils et bibliothèques python comme [scrapy](https://scrapy.org/) pour la collecte de données du WEB, [spacy](https://spacy.io/) pour le traitement du langage natuel ou  [scikit-learn](https://spacy.io/) pour l'analyse de données et l'apprentissage à partir de données ont aussi pu être utilisées.

Cette semaine consiste en la mise en applications des connaissances acquises lors de la première semaine pour une application autour de la marque CentraleSupélec. Pour ce projet, une bonne partie du projet de semaine 1 peut être réutilisé mais il faudra bien entendu proposer de nouvelles fonctionnalités notamment en terme de visualisation. Pour ce projet, chaque groupe choisira ou se constitura autour d'une ou plusieurs fonctionnalités qu'il faudra intégrer et présenter devant un jury à l'issue de cette semaine.


## CODING WEEK 08 - Portwiture

[Portwiture](http://portwiture.com/) est un projet qui permet de créer un portrait du compte Twitter d'un internaute ou de tout autre compte dont le nom est connu. Le principe est simple : à partir d'une analyse des tweets de l'utilisateur ciblé, les mots clés les plus fréquents sont utilisés pour requêter et trouver des images du site Flickr. Le résultat est une mosaïque formée de 50 images qui dresse un portrait de l'usage du compte Twitter. L'objectif de ce projet est de reproduire Portwiture en mieux. Les différentes équipes constituées travailleront sur différentes parties du projet et les intégreront pour un prototype fonctionnel à l'issue de la semaine et qui sera défendu devant un jury.

Lors de la première semaine, au travers du projet Twitter, vous avez appris à collecter et récupérer des données du WEB (avec [tweepy](http://www.tweepy.org/) ou avec la récupération de fichiers [json](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation)), à faire quelques analyses simples et opérations de traitement du langage naturel (avec [pandas](https://pandas.pydata.org/) et [Textblob](https://textblob.readthedocs.io/en/dev/)) et à produire des visualisations des résultats de vos analyses avec [matplotlib](https://matplotlib.org/), [seaborn](https://seaborn.pydata.org/) et [dash](https://plot.ly/products/dash/). 

D'autres outils et bibliothèques python comme [scrapy](https://scrapy.org/) pour la collecte de données du WEB, [spacy](https://spacy.io/) pour le traitement du langage natuel ou  [scikit-learn](https://spacy.io/) pour l'analyse de données et l'apprentissage à partir de données ont aussi pu être utilisées.

Quelques liens intéressants pour vous aider dans la réalisation de ce projet :

 + [API Flickr Python ](https://pypi.org/project/flickrapi/) et la documentation de l'[API Flickr](https://www.flickr.com/services/api/) officielle


## CODING WEEK 09 - Devenez un AI maker avec le kit vocal AIY !



Ce projet consiste à programmer une application d'intelligence artificielle avec le kit vocal [AIY](https://aiyprojects.withgoogle.com/voice/). Elle consistera en un challenge. Il s'agira de concevoir en équipe la meilleure application intelligente à l'aide du kit vocal AIY. Un jury élira la meilleure application. Bon courage !


## CODING WEEK 10 - Devenez un AI maker avec le kit vocal AIY !

Ce projet consiste à programmer une application d'intelligence artificielle avec le kit visuel [AIY](https://aiyprojects.withgoogle.com/vision/). Cela consistera en un challenge. Il s'agira de concevoir en équipe la meilleure application intelligente à l'aide du kit visuel AIY. Un jury élira la meilleure application.


## CODING WEEK 11 - 2048 et autres jeux ! (incluant FORUM)

L'objectif de ce projet est de construire un ensemble de jeux de type puzzle du type 2048.

Lors de la première semaine vous avez programmé de manière progressive le jeu 2048 et découvert un certain nombre de méthodologies et de bibliothèques python comme celles permettant de developper des interfaces graphiques (Tkinter) ou la bibliothèque pygame.


Lors de la deuxième semaine, il s'agira, sous la forme d'une [game jam](https://fr.wikipedia.org/wiki/Game_jam), par équipe de 4 à 6 personnes, de créer votre propre jeu de type 2048 comme par exemple le [2048 coins](http://2048coins.com/) pour découvrir les crypto-monnaies ou d'autres jeux de type puzzle comme par exemple [Threes!](http://asherv.com/threes/), [OpenFlood](https://play.google.com/store/apps/details?id=com.gunshippenguin.openflood&hl=fr) ou un jeu de votre invention.

Lors de la dernière journée, un jury jouera à vos différents jeux et élira le meilleur jeu du groupe !

## CODING WEEK 12 - Des jeux mathématiques  made in CentraleSupélec!

L'objectif de ce projet est de construire un ensemble de jeux mathématiques destinés aux élèves du primaire à l'instar de [calcul@tice](https://calculatice.ac-lille.fr/spip.php?rubrique2) ou de [netmaths](https://www.netmath.ca/fr-qc/) ou encore de [exploding dots](https://www.explodingdots.org/) mais avec vos propres idées et scénarios et en réutilisant les apprentissages de la semaine 1 pendant laquelle vous avez programmé de manière progressive un jeu 2048. 

Lors de cette deuxième semaine, chaque équipe, de 4 à 6 personnes, travaillera en la réalisation d'un ou plusieurs jeux. Les réalisations seront présentées lors d'un jury le vendredi 23 après midi.


## CODING WEEK 13 - Sudoku : Photographiez et résolvez! 


L'objectif de ce projet est de construire une application permettant de scanner un sudoku ou un jeu équivalent comme par exemple [Hidato](http://www.hidato.com/), de reconstruire la grille associée et d'en proposer une résolution ou à minima, de permettre à un joueur de jouer (avec retour arrière).

Lors de la première semaine vous avez programmé de manière progressive le jeu 2048 et découvert un certain nombre de méthodologies et de bibliothèques python comme celles permettant de developper des interfaces graphiques (Tkinter) ou la bibliothèque pygame qu'il vous faudra réutiliser.


Lors de la deuxième semaine, il s'agira, sous la forme d'une compétition (groupes de 4 à 6 personnes) de construire l'application scannant et resolvant un sodoku, un kakuro, hidato ou équivalent. Des grilles papier seront fournies chaque jour aux différents groupes pour se challenger.

Quelques références pour vous aider concernant la partie vision par ordinateur :

 + [OpenCV](https://opencv.org/) et [opencv python](https://pypi.org/project/opencv-python/).
 + Guide d'installation de OpenCV en python [ici](https://www.pyimagesearch.com/opencv-tutorials-resources-guides/) et [ici](https://www.scivision.co/install-opencv-python-windows/) pour Windows
 + Projets similaires, notamment pour vous aider sur la partie reconnaissance de la grille :
  + [http://www.shogun-toolbox.org/static/notebook/current/Sudoku_recognizer.html](http://www.shogun-toolbox.org/static/notebook/current/Sudoku_recognizer.html)
  + [https://caphuuquan.blogspot.com/2017/04/building-simple-sudoku-solver-from.html](https://caphuuquan.blogspot.com/2017/04/building-simple-sudoku-solver-from.html)
  +[https://www.instructables.com/id/Sudoku-Solver/](https://www.instructables.com/id/Sudoku-Solver/).
  
  
  
## CODING WEEK 14 - Détecteur d'insultes

Ce projet consiste en la conception d'un détecteur d'insultes dans les commentaires de sites web comme YouTube, Reddit ou les sites web d'organes de presse.

Lors de la première semaine, au travers du projet Twitter, vous avez appris à collecter et récupérer des données du WEB (avec [tweepy](http://www.tweepy.org/) ou avec la récupération de fichiers [json](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation)), à faire quelques analyses simples et opérations de traitement du langage naturel (avec [pandas](https://pandas.pydata.org/) et [Textblob](https://textblob.readthedocs.io/en/dev/)) et à produire des visualisations des résultats de vos analyses avec [matplotlib](https://matplotlib.org/), [seaborn](https://seaborn.pydata.org/) et [dash](https://plot.ly/products/dash/). 

D'autres outils et bibliothèques python comme [scrapy](https://scrapy.org/) pour la collecte de données du WEB, [spacy](https://spacy.io/) pour le traitement du langage natuel ou  [scikit-learn](https://spacy.io/) pour l'analyse de données et l'apprentissage à partir de données ont aussi pu être utilisées.


La deuxième semaine consiste en la réalisation du meilleur détecteur d'insultes en groupes de 4 à 6 personnes. En plus de savoir détecter si un commentaire est une insulte ou pas, vous ajouterez aussi des analyses et les visualisations associées pour permettre de voir comment une insulte impacte le reste des commentaires pour un billet donné. Un jury évaluera les différents projets en fin de semaine.

  
Quelques liens :

 + [Praw](https://praw.readthedocs.io/en/latest/#)
 + [YouTube API](https://medium.com/greyatom/youtube-data-in-python-6147160c5833)
 
Ou des liens vers des jeux de données si vous ne voulez pas passer par l'étape de création de compte.
 
 + [Reddit datasets](https://github.com/linanqiu/reddit-dataset)
 + [Reddit datasets] (https://www.kaggle.com/danofer/sarcasm)
 + ...

 
## CODING WEEK 15 - Applications web simples et utiles pour la vie quotidienne

Ce projet consiste à programmer des applications web simples mais utiles à l'instar des applications pour les comptes entre amis comme [Tricount](https://www.tricount.com/fr/faire-les-comptes-entre-amis), [Things](https://culturedcode.com/things/) pour les TO-DO listes, [Feedly](https://feedly.com/i/welcome) ou d'autres.

Lors de la première semaine vous avez appris à contruire et programmer un site web à l'aide du Framework Django. 

Lors de cette deuxième semaine, il vous faudra réutiliser les méthodes et les connaissances de la semaine 1 pour la réalisation d'une application web de votre choix, en équipe de 4 à 6,  dont l'objectif est de fournir un service pour améliorer notre quotidien. 

Votre travail sera évalué par un jury à la fin de cette semaine.
 
## CODING WEEK 16 - Des applications Web pour les coding weeks.

Ce projet consiste à améliorer l'organisation ainsi que le suivi de la coding weeks en concevant des outils pour faciliter sa mise en oeuvre. Les possibilités sont nombreuses comme par exemple la conception d'une plate-forme d'ideations de projets, un stackoverflow ou une FAQ dédié aux coding weeks ou encore des outils permettant de tester la qualité d'un code (commentaires, respect des conventions,...). D'autres idées sont tout à fait possibles. 

Lors de la première semaine vous avez appris à contruire et programmer un site web à l'aide du Framework Django. 

Lors de cette deuxième semaine, il vous faudra réutiliser les méthodes et les connaissances de la semaine 1 pour la réalisation d'une application web de votre choix, en équipe de 4 à 6,  dont l'objectif est d'ameliorer et de faciliter l'organisation de semaines de programmation en mode bootcamp.


## CODING WEEK 17 - Contribuez à l'open source !

L'objectif ici est de contribuer à l'open source au travers d'une part de la conception d'un projet de bibliothèque d'intelligence artificielle open-source en python et d'autre part par la résolution d'issues sur des bibliothèques connues comme simpy, scikit-image (langage python ou tout autres langage de votre choix que vous maîtrisez). On s'appuiera pour cela sur l'outil codetriage ou équivalent pour détecter les issues ouvertes et leur niveau de difficulté.


Lors de la première semaine, vous avez découvert des méthodologies de programmation au travers de l'implémentation incrémentale du jeu 2048. Vous avez aussi découvert le monde de l'open source en résolvant vos premières issues.


Pendant la deuxième semaine, chaque groupe traitera l'ajout d'un module conséquent de son choix sur la bibliothéque d'IA et traitera au moins une issue ouverte sur d'autres projets open-source de la sphère python ou emblématique de CentraleSupélec (vlc,...) ou de ParisSaclay (scikit-image, scikit-learn...). 

Un jury élira à la fin de la semaine le meilleur groupe contributeur à ces projets open-source.


## CODING WEEK 18 - Applications audio en python!

L'objectif de ce projet est de concevoir une application audio innovante de votre choix à l'aide d'une bibliothèque audio python. Vous travaillerez en groupe de 4 à 6 personnes sur l'application de votre choix et qui sera fonction du succès dans l'installation des biblithèques audio python. 

Un jury élira la meilleure application.

## CODING WEEK 19 - Une vitrine de simulations pour l'éducation


Ce projet consiste en la conception d'un zoo de simulations physiques pour les enfants en python comme par exemple la simulation et la visualisation de la trajectoire d'un projectile ou la simulation du pendule à l'aide des outils de simulation en python (numpy, simpy ...) et d'outils de visualisation comme pymunk.

Lors de la première semaine, vous n'avez pas forcement eu le temps d'utiliser [pymunk](http://www.pymunk.org/en/latest/tutorials/SlideAndPinJoint.html) et il ne sera donc pas obligatoire de l'utiliser lors de la semaine 2.

Par équipe de 4 à 6, votre objectif sera donc de simuler et de visualiser l'évolution d'un phénomène physique ou vivant à l'instar de ce que vous avez fait sur les automates cellulaires.


Quelques liens :

 + [zoologie automates](https://interstices.info/la-riche-zoologie-des-automates-cellulaires/)
 + [Science etonnante](https://sciencetonnante.wordpress.com/2013/10/28/les-automates-cellulaires-elementaires/)
 + [Musique et automates cellulaires](http://musiquealgorithmique.fr/automates-cellulaires/)
 + [Dunes étoiles](http://www.insu.cnrs.fr/terre-solide/lithosphere-continentale/les-automates-cellulaires-une-maniere-de-modeliser-les-systeme)
 + [Wolphram Alpha](https://www.wolframalpha.com/examples/science-and-technology/computational-sciences/cellular-automata/)
 + ...
 

Si vous voulez travailler avec pymunk, des idées de ce qu'il est possible de faire est disponible sur le site de pymunk [ici](http://www.pymunk.org/en/latest/examples.html) et [ici](http://www.pymunk.org/en/latest/showcase.html). Un petit tutorial est aussi disponible [ici](https://www.supinfo.com/articles/single/4777-pymunk-moteur-physique-2d-vos-programmes-python).






## CODING WEEK 20 - Automates cellulaires, jeu de la vie et autres applications

L'objectif de ce projet est d'appliquer les automates cellulaires, découverts et programmés lors de la première semaine à des applications réelles comme par exemple les [systolic arrays](https://en.wikipedia.org/wiki/Systolic_array) des architectures parallèles, [le gaz sur réseau](https://en.wikipedia.org/wiki/Lattice_gas_automaton) pour modéliser le comportement d'un fluide et bien d'autres.


Pendant la deuxième semaine, chaque groupe de 4 à 6 personnes, choisira une application réelle pouvant être modélisée et simulée à l'aide des automates cellulaires et l'implémentera à l'aide des outils de simulation et de visualisation de python en capitalisant au maximum sur les acquis de la semaine 1.

Quelques liens :

 + [zoologie automates](https://interstices.info/la-riche-zoologie-des-automates-cellulaires/)
 + [Science etonnante](https://sciencetonnante.wordpress.com/2013/10/28/les-automates-cellulaires-elementaires/)
 + [Musique et automates cellulaires](http://musiquealgorithmique.fr/automates-cellulaires/)
 + [Dunes étoiles](http://www.insu.cnrs.fr/terre-solide/lithosphere-continentale/les-automates-cellulaires-une-maniere-de-modeliser-les-systeme)
 + [Wolphram Alpha](https://www.wolframalpha.com/examples/science-and-technology/computational-sciences/cellular-automata/)
 + ...



## CODING WEEK 21 - Devenez un Tech for good explorer !


[Tech for Good Explorers](http://www.latitudes.cc/tech-for-good-explorers) est un programme pédagogique innovant, fondé par d'anciens de l'école, qui s'intègre aux écoles d'ingénieurs, informatique et universités et qui consiste à mobiliser les compétences de leurs étudiant.e.s, dans le cadre de leur cursus, afin de résoudre des challenges liés aux nouvelles technologies proposés par des structures qui portent un projet à fort impact social ou environnemental.



L'objectif de ce projet est de participer à 3 projets du programme Tech for Good Explorers actuellement mené par les étudiants de 3ème année de l'option ISIA : un projet pour [LabelEmmaüs](https://www.label-emmaus.co/fr/) pour le développement d'un algorithme de pricing, un projet d'analyse de données sur des lobbys d'intérêts pour la [Haute autorité sur la transparence de la vie publique](https://www.hatvp.fr/) et un projet d'analyse d'images pour [PowerCorner](http://powercorner.com/) pour l'implantation de réseaux électriques en Afrique.

Vous travaillerez donc, par équipe de 4 à 6, sur un des projets du programme Tech for Good Explorers de votre choix ou sur un projet similaire, en concevant et en codant une de ses briques en partenariat avec Latitudes et les 3A impliqués.


## CODING WEEK 23 - The Devoteam Visual Recognition Challenge


[Devoteam](https://france.devoteam.com/) delivers Innovative Technology Consulting for Business.

In the context of these CentraleSupélec Coding weeks, Devoteam proposes a challenge on visual recognition. 


During the second week, the challenge will consist in image captioning. An instagram account with some photos will be provided to each team and the goal of the project is to automatically add intelligent comments to the images. The quality of the generated comments will be use to choose the best projet. 

Some links in order to help you :

 + [Image recognition in python](https://towardsdatascience.com/tensorflow-image-recognition-python-api-e35f7d412a70)
 + [API Instagram python](https://github.com/facebookarchive/python-instagram)
 + [Image recognition in python] (https://towardsdatascience.com/object-detection-with-10-lines-of-code-d6cb4d86f606)



## CODING WEEK 24 et 25 - Doctolib Recruitment : review less, move faster 1


[Doctolib](https://www.doctolib.fr/), le leader français de la prise de rendez-vous médicaux en ligne ne cesse de grandir et a un fort besoin de recrutement. Rien que dans les six derniers mois, la start-up est passée de 350 collaborateurs à 700. Dans le processus de recrutement d'un ingénieur informatique chez Doctolib, le candidat au poste doit passer un test pour valider ses compétences techniques qui est ensuite corrigé et validé par les ingénieurs. Avec la croissance de l’entreprise, il est nécessaire de mettre en place une plate-forme pour faciliter cette étape. Ce projet consiste donc à programmer une plate-forme permettant de faciliter cette étape de recrutement en analysant par exemple le style, la duplication, la complexité et la couverture du code dans les tests des candidats. Idéalement, chaque candidat aurait un tableau de bord affichant ses différentes performances sur des critères à définir.


Lors de la première semaine, au travers du projet Twitter, vous avez appris à collecter et récupérer des données du WEB (avec [tweepy](http://www.tweepy.org/) ou avec la récupération de fichiers [json](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation)), à faire quelques analyses simples et opérations de traitement du langage naturel (avec [pandas](https://pandas.pydata.org/) et [Textblob](https://textblob.readthedocs.io/en/dev/)) et à produire des visualisations des résultats de vos analyses avec [matplotlib](https://matplotlib.org/), [seaborn](https://seaborn.pydata.org/) et [dash](https://plot.ly/products/dash/). 

D'autres outils et bibliothèques python comme [scrapy](https://scrapy.org/) pour la collecte de données du WEB, [spacy](https://spacy.io/) pour le traitement du langage natuel ou  [scikit-learn](https://spacy.io/) pour l'analyse de données et l'apprentissage à partir de données ont aussi pu être utilisées.


La deuxième semaine sera consacrée à la réalisation de la plate-forme pour Doctolib en équipe de 4 à 6 personnes. Chaque équipe s'occupera d'une partie du projet et les différents travaux devront être intégrés pour la constitution du produit final. 

Une petite compétition sera organisé entre le groupe 24 et 25. Le meilleur outil sera élu par les équipes de Doctolib.


## CODING WEEK 26 (FORUM)

Ce projet consiste à programmer une application d'intelligence artificielle avec un Raspberry Pi.
Cette deuxième semaine consistera en un challenge par équipe de 4 à 6 personnes. Il s'agira de concevoir en équipe la meilleure application intelligente à l'aide de votre Raspberry Pi.
Un jury élira la meilleure application à l'issue de la semaine.









